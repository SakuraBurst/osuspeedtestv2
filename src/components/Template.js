import React from 'react';
import { Container, Row, Col, Navbar } from 'react-bootstrap';
import logo from '../logo.png';

export default function Template({ children }) {
  return (
    <Container fluid={true}>
      <Row>
        <Col style={{ padding: 0 }}>
          <Navbar bg="dark" expand="lg">
            <Navbar.Brand href="#">
              <img
                src={logo}
                width="40"
                height="40"
                className="d-inline-block align-top"
                alt="click da sircle"
              />
            </Navbar.Brand>
          </Navbar>
        </Col>
      </Row>
      <main>
        <Row>
          <Col />
          <Col lg={5} style={{ display: 'flex', justifyContent: 'center' }}>
            {children}
          </Col>
          <Col />
        </Row>
      </main>
    </Container>
  );
}
