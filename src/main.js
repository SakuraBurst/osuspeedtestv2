/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import { Button } from 'react-bootstrap';
// todo
class MainApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      timerOn: false,
      timerStart: 0,
      timerTime: 0,
      clickNumbers: 0,
      firstButton: '',
      secondButton: '',
      streamTest: '',
      testState: false,
      bpm: 0
    };

    this.switch = this.switch.bind(this);
    this.firstButtonSeter = this.firstButtonSeter.bind(this);
    this.secondButtonSeter = this.secondButtonSeter.bind(this);
    this.numberOfClicksSeter = this.numberOfClicksSeter.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.startTimer = this.startTimer.bind(this);
    this.stopTimer = this.stopTimer.bind(this);
    this.stopTest = this.stopTest.bind(this);
  }

  startTimer() {
    this.setState({
      timerOn: true,
      timerTime: this.state.timerTime,
      timerStart: Date.now() - this.state.timerTime
    });
    this.timer = setInterval(() => {
      this.setState({
        timerTime: ((Date.now() - this.state.timerStart) / 1000).toFixed(3)
      });
    }, 10);
  }

  stopTimer() {
    this.setState({
      timerOn: false
    });
    clearInterval(this.timer);
  }

  stopTest() {
    this.stopTimer();
    this.setState({
      testState: false
    });
  }

  firstButtonSeter(e) {
    this.setState({
      firstButton: e.target.value
    });
  }

  secondButtonSeter(e) {
    this.setState({
      secondButton: e.target.value
    });
  }

  numberOfClicksSeter(e) {
    this.setState({
      clickNumbers: e.target.value
    });
  }

  switch() {
    this.setState({
      testState: true,
      streamTest: ''
    });
  }

  handleKeyPress(event) {
    if (!this.state.timerOn) {
      this.startTimer();
    }
    if (
      event.key === this.state.firstButton ||
      event.key === this.state.secondButton
    ) {
      this.setState(state => ({
        streamTest: state.streamTest + event.key
      }));
    }
    if (this.state.streamTest.length >= this.state.clickNumbers) {
      this.stopTimer();
    }
  }

  render() {
    const {
      firstButton,
      secondButton,
      clickNumbers,
      timerOn,
      bpm,
      streamTest,
      timerTime,
      testState,
      timerStart
    } = this.state;
    if (testState) {
      document.addEventListener('keydown', this.handleKeyPress);
    }
    if (streamTest.length >= clickNumbers) {
      document.removeEventListener('keydown', this.handleKeyPress);
    }
    if (timerOn) {
      this.state.bpm =
        Math.round(
          (((streamTest.length / (Date.now() - timerStart)) * 60000) / 4) * 100
        ) / 100;
    }
    return (
      <div className="osnova">
        <h3>OSU SPEED TEST</h3>
        <p>key 1</p>
        <input
          type="text"
          maxLength="1"
          value={firstButton}
          onChange={this.firstButtonSeter}
        />
        <p>key 2</p>
        <input
          type="text"
          maxLength="1"
          value={secondButton}
          onChange={this.secondButtonSeter}
        />
        <p>Number of clicks</p>
        <input
          type="number"
          value={clickNumbers}
          onChange={this.numberOfClicksSeter}
        />
        <br />

        <Button
          style={timerOn ? { display: 'none' } : {}}
          variant="outline-success"
          onClick={this.switch}
        >
          Start
        </Button>
        <Button
          style={timerOn ? {} : { display: 'none' }}
          variant="outline-danger"
          onClick={this.stopTimer}
        >
          Stop
        </Button>
        <h2>Your result</h2>
        <p>
          Tap Speed: {streamTest.length} taps/{timerTime} seconds
        </p>
        <p>Stream Speed: {bpm}</p>
      </div>
    );
  }
}
export default MainApp;
