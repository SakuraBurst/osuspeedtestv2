import React from 'react';
import MainApp from './main';
import Template from './components/Template';

function App() {
  return (
    <div className="App">
      <Template>
        <MainApp style={{}} />
      </Template>
    </div>
  );
}

export default App;
